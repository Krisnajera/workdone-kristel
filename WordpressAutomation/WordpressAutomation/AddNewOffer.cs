﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace WordpressAutomation
{
    public class AddNewOffer
    {
        public static void GotoAdd()
        {
            Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[1]/div/offer-search/div/div/div[3]/a")).Click();
        }


        //El combo box seleccionar y concatenar el string
        public static void AddGS1Prefix(string prefix)
        {
            
            Elements.ClickElementByXPath("//*[@id='gs1codeDropdownButton']");

            Elements.ClickElementByCssSelector("body > sea-app > offer-create > div > main-offer-form > form > div:nth-child(1) > div:nth-child(1) > div > div > ul > li:nth-child(" + prefix + ") > a");

        }
        //Alert para verificar que no se encuentra vacio la descripcion donde se muestra el prefijo 
        //seleccionado en el combo
        public static bool SearchGS1Prefix
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[1]/div[2]/div/input"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }

        }

        //Ingresar Offer Code
        public static void AddOfferCode(string offerercode)
        {
            Elements.SendKeysByCssSelector("body > sea-app > offer-create > div > main-offer-form > form > div:nth-child(2) > div.col-md-2.clearfix > input", offerercode);
        }
        //Asert para offer code
        public static bool AssertAddOfferCode(string offerercode)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[2]/div[1]/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == offerercode;  //el valor que se envia del send keys

        }

        //Boton de auto asignar
        public static void SelectAutoAssignCode()
        {
            Elements.ClickElementByXPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[2]/div[3]/button");
        }

        public static bool AssertSelectAutoAssignCode
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[2]/div[1]/input"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;
            }
        }

        //Ingresar Offerdescription
        public static void AddOfferDescritpion(string offerdescription)
        {
            Elements.SendKeysByXPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[2]/div[4]/input", offerdescription);
                                       
        }

        //Asert para offerdescription
        public static bool AssertAddOfferDescripcion(string offerdescription)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[2]/div[4]/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == offerdescription;
        }


        //Ingresar FaceValue
        public static void AddFaceValue(string facevalue)
        {
            Elements.SendKeysByXPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[3]/div/div[1]/div/input",facevalue);
        }

        //Asert para Ingresar FaceValue
        public static bool AssertFaceValue(string facevalue)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[3]/div/div[1]/div/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == facevalue;
        }

        //Escoger el checkbox de Free Offer

        public static void SelectFreeOfer()
        {
            Elements.ClickElementByXPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[3]/div/div[2]/div/input");
        }

        //Asert para verificar el checkbox
        public static bool AssertSelectOffer()
        {
            if (Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[3]/div/div[2]/div/input")).Selected)
                return true;
            return false;
        }


        //Agregar el minimun
        public static void Addminimun(string minimun)
        {
            Elements.SendKeysByXPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[3]/div/div[3]/div/input",minimun);
        }

        //Asert para el minimun
        public static bool AssertAddMinimun(string minumun)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[3]/div/div[3]/div/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == minumun;
        }


        //Agregar el maximun
        public static void Addmaximun(string maximun)
        {
            Elements.SendKeysByXPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[3]/div/div[4]/div/input", maximun);
        }

        //Asert para maximun
        public static bool AssertAddMaximum(string maximun)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[3]/div/div[4]/div/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == maximun;
        }

        //Agregar el PurchaseRequiremnets
        public static void AddPurchaseRequirements(string requirements)
        {
            Elements.SendKeysByXPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[3]/div/div[5]/input",requirements);
        }

        //Assert del PurchaseRequiremnets
        public static bool AssertPurchaseRequirements(string requirements)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[3]/div/div[5]/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == requirements;
        }


        //Verificar si se selecciona el checkbox de No Expiration Date

        public static void SelectNoExpirationDate()
        {
            Elements.ClickElementByXPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[3]/div/div[6]/div/label/input");
        }

        //Asert para verificar el checkbox de expirationDate
        public static bool AssertNoExpirationDate()
        {
            if (Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[3]/div/div[6]/div/label/input")).Selected)
                return true;
            return false;
        }


        //El combo box seleccionar y concatenar el string
        //Seleccionar el combo de Family Code
        public static void SelectFamilyCode(string prefix)
        {

            Elements.ClickElementByXPath("//*[@id='FamilyCodeDropdownButton']");

            Elements.ClickElementByCssSelector("body > sea-app > offer-create > div > main-offer-form > form > div:nth-child(4) > div.col-md-3 > div > ul > li:nth-child(" + prefix + ") > a");

        }
        //Alert para verificar que no se encuentra vacio la descripcion donde se muestra el prefijo 
        //seleccionado en el combo
        public static bool AssertFamilyCode
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[4]/div[2]/input"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }

        }


        //Agregar la fecha DropDate

        public static void AddDropDate(string dropdate)
        {
            Elements.SendKeysByXPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[4]/div[3]/div[1]/div/my-date-picker/div/div/input", dropdate);
        }

        //Asert para maximun
        public static bool AssertDropDate(string dropdate)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[4]/div[3]/div[1]/div/my-date-picker/div/div/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == dropdate;
        }


        //Presionar el boton Yes del cuadro de se ingresan FaceValue,Minimun,Maximun,Purchase Requiriments

        public static void Confirm()
        {
            //Elements.ClickElementByXPath("/html/body/bs-modal[4]/div/div/bs-modal-footer/div/div/a[2]");
            Elements.ClickElementByCssSelector("body > bs-modal.fade.modal.in > div > div > bs-modal-footer > div > div > a.btn.btn-primary");

        }

        //Evento para dar click para que se muestre el cuadro de confirmar

        public static void Click()
        {
            Elements.ClickElementByXPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[2]/div[4]/input");
        }

        //Asert para Confirmar y darle click al cuandro de confirmar cuando se ingresan FaceValue,Minimun,Maximun,Purchase Requiriments

        public static bool AsertConfirm
        {

            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[4]/div/div/bs-modal-header/div/h4"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }

        }

        //Añadir Expiration Date
        public static void AddExpirationDate(string expirationdate)
        {
            Elements.SendKeysByXPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[4]/div[3]/div[2]/div/my-date-picker/div/div/input",expirationdate);
        }

        //Asert para Expiration date
        public static bool AssertExpirationDate(string expirationdate)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-create/div/main-offer-form/form/div[4]/div[3]/div[2]/div/my-date-picker/div/div/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == expirationdate;
        }




        //Pedir el codigo de barras
        // ******************************** Order Barcode********************************************//

        //Click al bootn de orden barcode
        public static void Clickorderbarcode()
        {
            Elements.ClickElementByXPath("/html/body/sea-app/offer-create/div/div[2]/div/button");
        }

        //Asert para el boton comprobar si aparece el cuadro siguiente

        public static bool AsertClickOrdenBarcocde
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[1]/div/div/bs-modal-header/div/h4"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }


        //Barcode order preferences la otra pestaña que aparece cuando se da click en order barcode


        //Escoger FileType
        public static void AddFiletype(string filetype)
        {

            Elements.ClickElementByXPath("/html/body/bs-modal[1]/div/div/bs-modal-body/div/div[1]/div[1]/div/button");
            Elements.ClickElementByCssSelector("body > bs-modal.fade.modal.in > div > div > bs-modal-body > div > div:nth-child(1) > div.col-md-3 > div > ul > li:nth-child(" + filetype + ") > a");

        }

        //Asert para le file type

        public static bool AsertAddFiletype
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[1]/div/div/bs-modal-body/div/div[1]/div[2]/label"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }

        }

        //Añadir email barcode

        public static void AddEmailBarcode(string email)
        {
            Elements.SendKeysByCssSelector("body > bs-modal.fade.modal.in > div > div > bs-modal-body > div > div:nth-child(1) > div.col-md-9 > input",email);
        }



        public static bool AssertAddEmailBarcode(string email)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[1]/div/div/bs-modal-body/div/div[1]/div[2]/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == email;  //el valor que se envia del send keys

        }

        //Añadir Special Instructions

        public static void AddSpecialInstructions()
        {
            Elements.SendKeysByCssSelector("body > bs-modal.fade.modal.in > div > div > bs-modal-body > div > div:nth-child(2) > div > input", "hello");
        }

        //Assert para AddSpecialInstructions()

        public static bool AssertAddSpecialInstructions
        {

            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[1]/div/div/bs-modal-footer/div/div/button[2]"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }

        //Presionar el click del boton Next del cuadro Barcode Order Preferences

        public static void ClickButttonNext()
        {

            Elements.ClickElementByXPath("/html/body/bs-modal[1]/div/div/bs-modal-footer/div/div/button[2]");
        }


        //Asert para verificar el click del boton el asert se hace si abre la ventana siguiente.

        //Asert para el boton comprobar si aparece el cuadro siguiente

        public static bool AsertClickButtonNext
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[3]/div/div/bs-modal-header/div/h4"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }

        //Marcar el aceptar Condiciones
        public static void ClickAcceptConditions()
        {

            Elements.ClickElementByXPath("/html/body/bs-modal[3]/div/div/bs-modal-body/div/div[3]/div/div/label/span[2]");
        }

        //Assert para Aceptar Condiciones

        public static bool AssertClickAcceptConditions
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[3]/div/div/bs-modal-footer/div/div/button[2]"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }


        //Presionar el   Accept luego de aceptas las condiciones

        public static void ButtonAceptConditions()
        {

            Elements.ClickElementByXPath("/html/body/bs-modal[3]/div/div/bs-modal-footer/div/div/button[2]");
        }

        //Assert para validra que acepto las condiciones 

        public static bool AssertButtonAceptConditions
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-finish/div/div[1]/div/h3"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }


        //***    New Offer Marketing Information se divide en dos en una misma view ya para salvar el codigo de barras  ***//
        // Esta es   Budgeting Information //

        //Ingresar el Distribution #

        public static void AddDistribution(string distribution)
        {
            Elements.SendKeysByXPath("/html/body/sea-app/offer-finish/div/offer-marketing-form/div[2]/div[1]/input", distribution);
        }

        //Asert para  ingresar el Distribution #
        public static bool AssertAddDistribution(string distribution)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-finish/div/offer-marketing-form/div[2]/div[1]/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == distribution;
        }

        //Aparece por defecto entonces en este test no se selecciona nada Calculate Budget by %

        //Ingresar Enter budget $

        public static void AddEnterbudget(string budget)
        {

            Elements.SendKeysByXPath("/html/body/sea-app/offer-finish/div/offer-marketing-form/div[2]/div[3]/div[1]/input", budget);
        }

        //Asert para  ingresar el Enter budget $
        public static bool AssertAddEnterbudget(string budget)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-finish/div/offer-marketing-form/div[2]/div[3]/div[1]/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == budget;
        }


        //Ingresar Creative Cost

        public static void AddCreativeCost(string creativecost)
        {

            Elements.SendKeysByXPath("/html/body/sea-app/offer-finish/div/offer-marketing-form/div[3]/div[1]/div/input", creativecost);
        }

        //Asert para ingresar Creative Cost
        public static bool AssertCreativeCost(string creativecost)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-finish/div/offer-marketing-form/div[3]/div[1]/div/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == creativecost;
        }

        //Ingresar Print Cost

        public static void AddPrintCost(string printcost)
        {

            Elements.SendKeysByXPath("/html/body/sea-app/offer-finish/div/offer-marketing-form/div[3]/div[2]/div/input", printcost);
        }

        //Asert para Print Cost
        public static bool AssertPrintCost(string printcost)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-finish/div/offer-marketing-form/div[3]/div[2]/div/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == printcost;
        }

        //Informacion Marketing Information dentro de la misma view de Budgeting Information

        //El combo box
        //Seleccionar Media Type
        public static void AddMediaType(string mediatype)
        {

            Elements.ClickElementByXPath("/html/body/sea-app/offer-finish/div/offer-marketing-form/div[5]/div[1]/div/button");

            Elements.ClickElementByCssSelector("body > sea-app > offer-finish > div > offer-marketing-form > div:nth-child(7) > div:nth-child(1) > div > ul > li:nth-child(" + mediatype + ") > a");

        }

        //Alert paraverificar que se selecciono correctamente el combo box de media type
        public static bool AssertMediaType
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-finish/div/offer-marketing-form/div[5]/div[2]/label"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }

        //Ingresar Event Description

        public static void AddEventDescription(string description)
        {

            Elements.SendKeysByXPath("/html/body/sea-app/offer-finish/div/offer-marketing-form/div[5]/div[2]/input",description);
        }

        //Asert para ingresar Event Description
        public static bool AssertEventDescription(string description)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-finish/div/offer-marketing-form/div[5]/div[2]/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == description;
        }

        //Click al boton save


        public static void ClickButtonSave()
        {

            Elements.ClickElementByXPath("/html/body/sea-app/offer-finish/div/div[2]/div[2]/button");
        }


        //Asert para verificar el click del boton el asert se hace si abre la ventana siguiente.

        //Asert para el boton comprobar si aparece el cuadro siguiente

        public static bool AsertClickButtonSave
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-edit/div/div[1]/div/h2/u"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }


        //Para cambia le estado del combo para que se Calculate Budget by $ lo demas sigue igual

        public static void SelectCalBudgetdollars()
        {
            Elements.ClickElementByXPath("/html/body/sea-app/offer-finish/div/offer-marketing-form/div[2]/div[2]/div[2]/label");
        }

        //Asert para verificar el checkbox de Calcular budget por porcentaje para que haga el calculo.
        public static bool AssertSelectCalBudgetdollars()
        {
            if (Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-finish/div/offer-marketing-form/div[2]/div[3]/div[1]/div/input")).Selected)
                return true;
            return false;

        }
    }
}
