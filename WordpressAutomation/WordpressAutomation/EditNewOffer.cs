﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;


namespace WordpressAutomation
{
   public class EditNewOffer
    {
     
          //** Boton para editar //

          public static void ClickButtonEdit()
          {

              Elements.ClickElementByXPath("/html/body/sea-app/offer-edit/div/div[4]/div[2]/div/div/button");
                                             
          }


          //Asert para verificar el click del boton el asert se hace si abre la ventana siguiente.

          //Asert para el boton comprobar si aparece el cuadro siguiente

          public static bool AsertClickButtonEdit
          {
              get
              {
                  try
                  {
                      Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-edit/div/div[1]/div/h2/u"));

                  }
                  catch (NoSuchElementException e)
                  {
                      return false;
                  }
                  return true;

              }
          }


     //  ********************************EDITAR*************************************
          public static void EditOfferDescritpion(string offerdescription)
          {
              Elements.SendKeysByXPath("/html/body/sea-app/offer-edit/div/main-offer-form/form/div[2]/div[4]/input", offerdescription);
          } 

          //Editar para Offerdescripcion
          public static bool AssertEditOfferDescripcion(string offerdescription)
          {
              IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-edit/div/main-offer-form/form/div[2]/div[4]/input"));
              string getValue = TargetElement.GetAttribute("value");          
              Console.WriteLine(getValue);
              return getValue == offerdescription;
          }


          public static void EditDropDate(string dropdate)
          {
              Elements.SendKeysByXPath("/html/body/sea-app/offer-edit/div/main-offer-form/form/div[4]/div[3]/div[1]/div/my-date-picker/div/div/input", dropdate);
          }

          //Asert para dropdate
          public static bool AssertEditDropDate(string dropdate)
          {
              IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-edit/div/main-offer-form/form/div[4]/div[3]/div[1]/div/my-date-picker/div/div/input"));
              string getValue = TargetElement.GetAttribute("value");           
              Console.WriteLine(getValue);
              return getValue == dropdate;
          }


       //Number Distribution
          public static void EditDistribution(string distribution)
          {
              Elements.SendKeysByXPath("/html/body/sea-app/offer-edit/div/offer-marketing-form/div[2]/div[1]/input", distribution);
          }

          //Asert para editar el Distribution #
          public static bool AssertEditDistribution(string distribution)
          {
              IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-edit/div/offer-marketing-form/div[2]/div[1]/input"));
              string getValue = TargetElement.GetAttribute("value");
              Console.WriteLine(getValue);
              return getValue == distribution;
          }

       //EditarEnterBudget
          public static void EditEnterbudget(string budget)
          {

              Elements.SendKeysByXPath("/html/body/sea-app/offer-edit/div/offer-marketing-form/div[2]/div[3]/div[1]/input", budget);
          }
                                               
          //Asert para  editar el Enter budget $
          public static bool AssertEditEnterbudget(string budget)
          {
              IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-edit/div/offer-marketing-form/div[2]/div[3]/div[1]/input"));
              string getValue = TargetElement.GetAttribute("value");
              Console.WriteLine(getValue);
              return getValue == budget;
          }

       //Editar Creative Cost
          public static void EditCreativeCost(string creativecost)
          {

              Elements.SendKeysByXPath("/html/body/sea-app/offer-edit/div/offer-marketing-form/div[3]/div[1]/div/input", creativecost);
          }

          //Asert para editar Creative Cost
          public static bool AssertEditCreativeCost(string creativecost)
          {
              IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-edit/div/offer-marketing-form/div[3]/div[1]/div/input"));
              string getValue = TargetElement.GetAttribute("value");
              Console.WriteLine(getValue);
              return getValue == creativecost;
          }

       //EditarPrintCost
          public static void EditPrintCost(string printcost)
          {

              Elements.SendKeysByXPath("/html/body/sea-app/offer-edit/div/offer-marketing-form/div[3]/div[2]/div/input", printcost);
          }

          //Asert para Editar Print Cost
          public static bool AssertEditPrintCost(string printcost)
          {
              IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-edit/div/offer-marketing-form/div[3]/div[2]/div/input"));
              string getValue = TargetElement.GetAttribute("value");
              Console.WriteLine(getValue);
              return getValue == printcost;
          }

       //EditarMediaType
          public static void EditMediaType(string mediatype)
          {

              Elements.ClickElementByXPath("/html/body/sea-app/offer-edit/div/offer-marketing-form/div[5]/div[1]/div/button");

              Elements.ClickElementByCssSelector("body > sea-app > offer-edit > div > offer-marketing-form > div:nth-child(7) > div:nth-child(1) > div > ul > li:nth-child(" + mediatype + ") > a");

          }

          //Alert paraverificar que se selecciono correctamente el combo box de media type
          public static bool AssertEditMediaType
          {
              get
              {
                  try
                  {
                      Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-edit/div/offer-marketing-form/div[5]/div[1]/div/button"));

                  }
                  catch (NoSuchElementException e)
                  {
                      return false;
                  }
                  return true;

              }
          }

       //EditarEventDescriuption
          public static void EditEventDescription(string description)
          {

              Elements.SendKeysByXPath("/html/body/sea-app/offer-edit/div/offer-marketing-form/div[5]/div[2]/input", description);
          }

          //Asert para editar Event Description
          public static bool AssertEditEventDescription(string description)
          {
              IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-edit/div/offer-marketing-form/div[5]/div[2]/input"));
              string getValue = TargetElement.GetAttribute("value");
              Console.WriteLine(getValue);
              return getValue == description;
          }


      
          //** Boton para salvar //

          public static void ClickButtonSaveChanges()
          {

              Elements.ClickElementByXPath("/html/body/sea-app/offer-edit/div/div[4]/div[2]/div/button");

          }


          //Asert para verificar el click del boton el asert se hace si abre la ventana siguiente.

          //Asert para el boton comprobar si aparece el cuadro siguiente

          public static bool AsertClickButtonSaveChanges
          {
              get
              {
                  try
                  {
                      Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-edit/div/div[1]/div/h2/u"));

                  }
                  catch (NoSuchElementException e)
                  {
                      return false;
                  }
                  return true;

              }
          }


       //Boton para salvar en el siguiente formulario
          public static void ClickButtonReturnManageOfers()
          {

              Elements.ClickElementByXPath("/html/body/sea-app/offer-edit/div/div[4]/div[2]/div/div/a");

          }


          //Asert para verificar el click del boton el asert se hace si abre la ventana siguiente.

          //Asert para el boton comprobar si aparece el cuadro siguiente

          public static bool AssertClickButtonReturnManageOfers
          {
              get
              {
                  try
                  {
                      Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[1]/div/offer-search/div/div/div[2]/h3"));

                  }
                  catch (NoSuchElementException e)
                  {
                      return false;
                  }
                  return true;

              }
          }
     }
}



