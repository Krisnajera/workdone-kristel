﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordpressAutomation;


namespace WordpressTests
{
     [TestClass]
    public class MainTest : WordpressTest
    {
         [TestMethod]
         public void NavigateTabs()
         {

            // Assert.IsTrue(DashboardPage.IsAt, "Failed to login");
          
             MainPage.GoToInvoiecSearch();
             Assert.IsTrue(MainPage.AssertInvoiceSearch(), "Failed to access Invoice Search");

             MainPage.GoToReports();
             Assert.IsTrue(MainPage.AssertReport(), "Failed to access Report");

             MainPage.GoToManageUsers();
             Assert.IsTrue(MainPage.AssertManageUsers(), "Failed to access Manage Users");

             MainPage.GoToManageFamilyCodes();
             Assert.IsTrue(MainPage.AssertManageFamilyCodes(), "Failed to access Manage Family Codes");

             MainPage.GoToManageGS1Prefixes();
             Assert.IsTrue(MainPage.AssertManageGS1Prefixes(), "Failed to access Manage GS1 Prefixes");

             MainPage.GoToManageOffers();
             Assert.IsTrue(MainPage.AssertManageOffers(), "Failed to access Manage Offers");

         }

    }
}
