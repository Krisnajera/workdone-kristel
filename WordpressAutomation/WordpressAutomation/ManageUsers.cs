﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace WordpressAutomation
{
     public class ManageUsers
    {
         //Ingresar al ManageUsers en el menu
        public static void GotoManageUsers()
        {
            Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/ul/li[4]/a")).Click();
        }

        //Alert para verificar si abre a ptra ventana de ManageUsers
        public static bool AsertGotoManageUsers
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[3]/div/div/form/div/div[1]/h4"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }


         //Ingresar al boton de "mas" para ingrsar usuarios
         public static void GotoUserCreation()
         {
             Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[4]/div/account-manager/user-table/div/div/div/div/a")).Click();
         }

         //Alert para verificar si abre a ptra ventana.
         public static bool AsertGotoUserCreation

         {
             get
             {
                 try
                 {
                     Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[3]/div/div/form/div/div[1]/h4"));

                 }
                 catch (NoSuchElementException e)
                 {
                     return false;
                 }
                 return true;

             }
         }


         //Ingresar first name
         public static void AddFirstName(string firstaname)
         {
             Elements.SendKeysByXPath("//*[@id='inputFirstName']", firstaname);
         }
         //Asert para ingresar first name
         public static bool AssertAddFirstName (string firstaname)
         {
             IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("//*[@id='inputFirstName']"));
             string getValue = TargetElement.GetAttribute("value");
             Console.WriteLine(getValue);
             return getValue == firstaname;     //el valor que se envia del send keys
         }


         //Ingresar LastaName
         public static void AddLastName(string lastname)
         {
             Elements.SendKeysByXPath("//*[@id='inputLastName']", lastname);
         }
         //Asert para ingresar Lastname
         public static bool AssertAddLastName(string lastname)
         {
             IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("//*[@id='inputLastName']"));
             string getValue = TargetElement.GetAttribute("value");
             Console.WriteLine(getValue);
             return getValue == lastname;     //el valor que se envia del send keys
         }



         //Ingresar Password
         public static void AddPassword(string password)
         {
             Elements.SendKeysByXPath("//*[@id='inputPassword']", password);
         }
         //Asert para ingresar Password
         public static bool AssertAddPassword(string password)
         {
             IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("//*[@id='inputPassword']"));
             string getValue = TargetElement.GetAttribute("value");
             Console.WriteLine(getValue);
             return getValue == password;     //el valor que se envia del send keys
         }
      


         //Ingresar PasswordConfirm
         public static void AddPasswordConfirm(string passwordconfirm)
         {
             Elements.SendKeysByXPath("//*[@id='inputPasswordConfirm']", passwordconfirm);
         }
         //Asert para ingresar Password
         public static bool AssertAddPasswordConfirm(string passwordconfirm)
         {
             IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("//*[@id='inputPasswordConfirm']"));
             string getValue = TargetElement.GetAttribute("value");
             Console.WriteLine(getValue);
             return getValue == passwordconfirm;     //el valor que se envia del send keys
         }


         //Ingresar Email
         public static void AddEmail(string email)
         {
             Elements.SendKeysByXPath("//*[@id='inputEmail']", email);
         }
         //Asert para ingresar Email
         public static bool AssertAddEmail(string email)
         {
             IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("//*[@id='inputEmail']"));
             string getValue = TargetElement.GetAttribute("value");
             Console.WriteLine(getValue);
             return getValue == email;     //el valor que se envia del send keys
         }


        //Elegir el type Type of Account

         public static void AddTypeAcount(string prefix)
         {

             Elements.ClickElementByXPath("/html/body/bs-modal[3]/div/div/form/div/div[2]/div[4]/div/div/a");

             Elements.ClickElementByCssSelector("body > bs-modal.fade.modal.in > div > div > form > div > div.modal-body > div:nth-child(6) > div > div > ul > li:nth-child(" + prefix + ") > a");
     
         }
         //Alert para el type of Acount
         public static bool AssertAddTypeAcount
         {
             get
             {
                 try
                 {
                     Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[3]/div/div/form/div/div[2]/div[4]/div/div/a"));

                 }
                 catch (NoSuchElementException e)
                 {
                     return false;
                 }
                 return true;

             }
         }

             //Click al boton submit para guardar el usuario creado
              public static void ClickButtonSaveUser()
        {

            Elements.ClickElementByXPath("/html/body/bs-modal[3]/div/div/form/div/div[3]/div/button");
        }


           //Asert para el click para guardar el usuario creado
        public static bool AsertClickButtonSaveUser 
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[4]/div/account-manager/div[1]/div/h3"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }


         ///////***********************UPDATE USER ********************************


         //va  a lalista de usuario y lo selecciona para abrir un mdal para editar y eliminar
        public static void GotolistForupdate()
        {
            Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[4]/div/account-manager/user-table/div/table/tbody/tr[16]/td[1]/a")).Click();
        }

        //Alert para verificar si abre a ptra ventana de ManageUsers
        public static bool AsertGotolistForupdate
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[3]/div/div/form/div/div[1]/h4"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }


        //Editar First Name
        public static void EditFirstName(string firstanme)
        {

            Elements.SendKeysByXPath("//*[@id='inputFirstName']", firstanme);
        }


        //Asert para  editar First name
        public static bool AssertEditFirstName(string firstanme)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("//*[@id='inputFirstName']"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == firstanme;
        }



        //Editar Last  Name
        public static void EditLastName(string lastname)
        {
           
            Elements.SendKeysByXPath("//*[@id='inputLastName']", lastname);
        }


        //Asert para  editar lastname
        public static bool AssertEditLastName(string lastname)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("//*[@id='inputLastName']"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == lastname;
        }


        //Editar  Type of Account
        public static void EditTypeofAcount(string typeaccount)
        {

            Elements.ClickElementByXPath("/html/body/bs-modal[3]/div/div/form/div/div[2]/div[3]/div[1]/div/a");

            Elements.ClickElementByCssSelector("body > bs-modal.fade.modal.in > div > div > form > div > div.modal-body > div:nth-child(5) > div.col-md-4 > div > ul > li:nth-child(" + typeaccount + ") > a");

        }
        //Asert paraverificar que se selecciono correctamente el combo box de Type Acount
        public static bool AssertEditTypeofAcount
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[3]/div/div/form/div/div[2]/div[3]/div[1]/div/a"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }


         //Seleccionar la opcion  para cambiar el password

        public static void GotoEditPassword()
        {
            Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[3]/div/div/form/div/div[2]/div[3]/div[3]/a[1]")).Click();
        }

        //Alert para verificar si se cambia el password
        public static bool AsertGotoEditPassword
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[5]/div/div/form/bs-modal-header/div/h4"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }


        //Ingresar Password para cambiarlo
        public static void EditPassword(string password)
        {
            Elements.SendKeysByXPath("//*[@id='inputPasswordModal']", password);
        }

        //Asert para ingresar Password
        public static bool AssertEditPassword(string password)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("//*[@id='inputPasswordModal']"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == password;     //el valor que se envia del send keys
        }



        //Ingresar PasswordConfirm para cambairlo
        public static void EditPasswordConfirm(string passwordconfirm)
        { 
            Elements.SendKeysByXPath("//*[@id='inputPasswordConfirmModal']", passwordconfirm);
        }
        //Asert para ingresar Password
        public static bool AssertEditPasswordConfirm(string passwordconfirm)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("//*[@id='inputPasswordConfirmModal']"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == passwordconfirm;     //el valor que se envia del send keys
        }


        public static void ClickButtonYesEditpassword()
        {
            Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[5]/div/div/form/bs-modal-footer/div/div/button[2]")).Click();
        }

        //Alert para verificar si abre a ptra ventana de ManageUsers
        public static bool AsertClickButtonYesEditpassword
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[3]/div/div/form/div/div[2]/div[4]/div[1]/div[1]/label"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }

        ///////***********************FIN UPDATE USER ********************************




        //Escribir Assigned Clients solo se seleciona en los roles de suario y read-user

        public static void SelectedAssignedClients(string assignedclients)
        {
            Elements.SendKeysByXPath("//*[@id='dropdownListClients']", assignedclients);
        }

         //Asert para AsignClients
        public static bool AssertSelectedAssignedClients(string assignedclients)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("//*[@id='dropdownListClients']"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == assignedclients;
        }
        

         //Click al boton Update

        public static void ClickButtonUpdate()
        {

            Elements.ClickElementByXPath("/html/body/bs-modal[3]/div/div/form/div/div[3]/div/button");
        }


        //Asert para el click al boton update
        public static bool AsertClickButtonUpdate
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[4]/div/account-manager/div[1]/div/h3"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }

        ///////***********************DELETE USER ********************************


         //ir al la lista de usuarios
        public static void GotolistFordelete()
        {
            Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[4]/div/account-manager/user-table/div/table/tbody/tr[18]/td[1]/a")).Click();
        }

        //Alert para verificar si abre a ptra ventana de ManageUsers
        public static bool AsertGotolistFordelete
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[3]/div/div/form/div/div[1]/h4"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }

         //Click al boton de eliminar 
        public static void ClickButtonEliminar()
        {
            Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[3]/div/div/form/div/div[2]/div[3]/div[3]/a[2]")).Click();
        }

        //Alert para verificar si abre a para eliminar
        public static bool AsertClickButtonEliminar
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[4]/div/div/bs-modal-header/div/h4"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }
         //Click al boton para confirmar eliminar el usuario

        public static void ClickConfirmDeleteUser()
        {
            Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[4]/div/div/bs-modal-footer/div/div/a[2]")).Click();
        }
         
        //Alert para darle Click al boton para confirmar eliminar el usuario
        public static bool AsertConfirmClickDeleteUser
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[4]/div/account-manager/div[1]/div/h3"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }




    }
}




