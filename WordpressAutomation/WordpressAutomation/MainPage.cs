﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordpressAutomation
{
  public   class MainPage
    {

      public static void GoToInvoiecSearch()
      {
          Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/ul/li[2]/a")).Click();

      }

      public static void GoToReports()
      {
          Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/ul/li[3]/a")).Click();

      }

      public static void GoToManageUsers()
      {
          Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/ul/li[4]/a")).Click();

      }

      public static void GoToManageFamilyCodes()
      {
          Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/ul/li[5]/a")).Click();
      }

      public static void GoToManageGS1Prefixes()
      {
          Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/ul/li[6]/a")).Click();
      }

      public static void GoToManageOffers()
      {
          Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/ul/li[1]/a")).Click();

      }

     //Aserts

       public static bool AssertInvoiceSearch()
       {

           if (Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[2]/div/invoice-search/div/div[2]/h3")).Displayed)
               return true;
           return false;

       }

       public static bool AssertReport()
       {

           if (Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[3]/div/activity-report/div/div[1]/div[2]/div/a[1]")).Displayed)
               return true;
           return false;

       }

       public static bool AssertManageUsers()
       {

           if (Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[4]/div/account-manager/h3")).Displayed)
               return true;
           return false;

       }

       public static bool AssertManageFamilyCodes()
       {

           if (Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[5]/div/family-code-manager/div/div[1]/div/h3")).Displayed)
               return true;
           return false;

       }

       public static bool AssertManageGS1Prefixes()
       {

           if (Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[6]/div/gs1code-manager/div/div[1]/div/h3")).Displayed)
               return true;
           return false;

       }

       public static bool AssertManageOffers()
       {

           if (Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[1]/div/offer-search/div/div/div[2]/h3")).Displayed)

               return true;
           return false;
       }

    }
}
